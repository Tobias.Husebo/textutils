package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new myTextAligner();

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
	}

	@Test
	void testFlushRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("  foo", aligner.flushRight("foo", 5));
	}

	@Test
	void testFlushLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("foo  ", aligner.flushLeft("foo", 5));
	}

	@Test
	void testWidthLongerThanWordCenter(){
		IllegalArgumentException thrown = assertThrows(
			IllegalArgumentException.class,
			() -> aligner.center("abc",2),
			"Expected aligner.center() to throw, but it didn't"
		);
 
	 	assertTrue(thrown.getMessage().contentEquals("Word longer than width"));
	}

	@Test
	void testWidthLongerThanWordRightFlush(){
		IllegalArgumentException thrown = assertThrows(
			IllegalArgumentException.class,
			() -> aligner.center("abc",2),
			"Expected aligner.center() to throw, but it didn't"
		);
 
	 	assertTrue(thrown.getMessage().contentEquals("Word longer than width"));
	}

	@Test
	void testWidthLongerThanWordLeftFlush(){
		IllegalArgumentException thrown = assertThrows(
			IllegalArgumentException.class,
			() -> aligner.center("abc",2),
			"Expected aligner.center() to throw, but it didn't"
		);
 
	 	assertTrue(thrown.getMessage().contentEquals("Word longer than width"));
	}
}
