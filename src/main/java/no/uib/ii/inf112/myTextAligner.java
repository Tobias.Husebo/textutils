package no.uib.ii.inf112;

public class myTextAligner implements TextAligner {

    @Override
    public String center(String text, int width) {
        if (text.length() > width){
            throw new IllegalArgumentException("Word longer than width");
        } else {
            int extra = (width - text.length()) / 2;
            return " ".repeat(extra) + text + " ".repeat(extra);
        }
    }

    @Override
    public String flushRight(String text, int width) {
        if (text.length() > width){
            throw new IllegalArgumentException("Word longer than width");
        } else {
            return " ".repeat(width-text.length()) + text;
        }
    }

    @Override
    public String flushLeft(String text, int width) {
        if (text.length() > width){
            throw new IllegalArgumentException("Word longer than width");
        } else {
            return text + " ".repeat(width-text.length());
        }
    }

    @Override
    public String justify(String text, int width) {
        

        return null;
    }
    
}
